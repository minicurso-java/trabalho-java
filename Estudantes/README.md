# Sistema de Gerenciamento de Estudantes

O Sistema de Gerenciamento de Estudantes é um projeto prático que simula um sistema de gerenciamento de estudantes real. Ele aplica os conceitos de herança, polimorfismo, abstração e encapsulamento.

## Classes

O sistema contém as seguintes classes:

- **Estudante**: Esta classe representa um estudante. Ela contém atributos como nome, ID do estudante, cursos matriculados, notas, etc.

- **Curso**: Esta classe representa um curso. Ela contém atributos como nome do curso, ID do curso, estudantes matriculados, etc.

- **Matrícula**: Esta classe representa uma matrícula de estudante em um curso. Ela contém atributos como ID da matrícula, estudante, curso, etc.

## Funcionalidades

O sistema fornece as seguintes funcionalidades:

- **Matrícula em cursos**: Os estudantes podem se matricular em cursos. O sistema registra a matrícula e atualiza a lista de estudantes do curso.

- **Visualização de notas**: Os estudantes podem visualizar suas notas para os cursos em que estão matriculados. O sistema retorna uma lista de notas para o estudante.

- **Gerenciamento de estudantes**: O sistema permite adicionar, atualizar e remover estudantes.

- **Gerenciamento de cursos**: O sistema permite adicionar, atualizar e remover cursos.

## Como usar

1. Inicie o sistema.
2. Selecione uma opção do menu principal.
3. Siga as instruções na tela para usar cada funcionalidade.