import java.util.ArrayList;
import java.util.List;

class Estoque {             //criamos a classe Estoque
    private List<Produto> produtos;

    public Estoque() {
        this.produtos = new ArrayList<>();
        //lista para armazenar os produtos
    }

    public void adicionarProduto(Produto produto) {
        this.produtos.add(produto);
        //add produtos
    }

    public Produto buscarProduto(int codigo) {
        for (Produto produto : this.produtos) {
            if (produto.getCodigo() == codigo) {
                return produto;
                //buscar o produto
            }
        }
        return null;
    }

    public void removerProduto(int codigo) {
        Produto produto = this.buscarProduto(codigo);
        if (produto != null) {
            this.produtos.remove(produto);
        }
        // remove o produto da lista de produtos
    }

    public void imprimirEstoque() {
        for (Produto produto : this.produtos) {
            System.out.println("Código: " + produto.getCodigo());
            System.out.println("Nome: " + produto.getNome());
            System.out.println("Preço: " + produto.getPreco());
            System.out.println("Quantidade: " + produto.getQuantidade());
            System.out.println("--------------------");
        }
        //print do estoque
    }

    public int getQuantidadeProdutos() {
        return this.produtos.size();//retona a quantidade de produtos
    }

    public int gerarCodigoProduto() {
        return this.getQuantidadeProdutos() + 1;//gera o código do produto
    }
}
