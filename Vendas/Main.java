import java.util.Scanner;//importa a classe Scanner
import java.util.ArrayList;//importa a classe ArrayList que é usada para armazenar os produtos
import java.util.List;//importa a classe List que é usada para armazenar os produtos

public class Main {//criação da classe Main aonde roda tudo
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);//criação do objeto scanner
    Estoque estoque = new Estoque();//criação do objeto estoque
    Caixa caixa = new Caixa();//criação do objeto caixa        
    System.out.println("Bem-vindo ao mercado!");
    while (true) {
      System.out.println("Você é um gerente ou um vendedor? (Digite 'sair' para encerrar)");
      String usuario = sc.nextLine().toLowerCase();
      if (usuario.equals("sair")) {
        break;
      }
      if (usuario.equals("gerente")) {
        if (autenticarGerente(sc)) {
      gerenciarEstoqueOuCaixa(sc, estoque, caixa);      
        } else {  
          System.out.println("Nome ou senha incorretos.");            
        }           
      } else if (usuario.equals("vendedor")) {                
        if (autenticarVendedor(sc)) {                   
          gerenciarVendasOuEstoque(sc, estoque, caixa);               
        } else {               
          System.out.println("Nome ou senha incorretos.");            
        }          
      } else {             
        System.out.println("Tipo de usuário inválido.");           
      }      
    }       
    sc.close();       
    System.out.println("Programa encerrado.");  
  }  
  private static boolean autenticarGerente(Scanner scanner) {       
    //ver se a senha e o nome batem com o do gerente (SENHA E USUÁRIO: admin)       
    System.out.println("Digite o nome do gerente:");        
    String nomeGerente = scanner.nextLine();       
    System.out.println("Digite a senha do gerente:");        
    String senhaGerente = scanner.nextLine();        
    return nomeGerente.equals("admin") && 
      senhaGerente.equals("admin");    
  }    
  private static boolean autenticarVendedor(Scanner scanner) {        
    //ver se a senha e o nome batem com o do vendedor (SENHA E USUÁRIO: vendedor)        
    System.out.println("Digite o nome do vendedor:");            
    String nomeVendedor = scanner.nextLine();       
    System.out.println("Digite a senha do vendedor:");        
    String senhaVendedor = scanner.nextLine();        
    return nomeVendedor.equals("vendedor") && senhaVendedor.equals("vendedor");
  } 
  private static void gerenciarEstoqueOuCaixa(Scanner scanner,Estoque estoque, Caixa caixa) {
    while (true) {
      System.out.println("Escolha uma opção:");            
      System.out.println("1. Gerenciar estoque");            
      System.out.println("2. Ir para o caixa");          
      System.out.println("3. Voltar");            
      int opcao = Integer.parseInt(scanner.nextLine());            
      switch (opcao) {               
        case 1:                    
          gerenciarEstoque(scanner, estoque);                   
          break;                
        case 2:                   
          realizarVendas(scanner, estoque, caixa);                  
          break;               
        case 3:                
          return;               
        default:                
          System.out.println("Opção inválida.");           
      }       
    }    
  }   
  private static void gerenciarVendasOuEstoque(Scanner scanner, Estoque estoque, Caixa caixa) {      
    while (true) {          
      System.out.println("Escolha uma opção:");           
      System.out.println("1. Realizar vendas");           
      System.out.println("2. Ver estoque");           
      System.out.println("3. Voltar");           
      int opcao = Integer.parseInt(scanner.nextLine());            
      switch (opcao) {            
        case 1:                
          realizarVendas(scanner, estoque, caixa);               
          break;              
        case 2:               
          estoque.imprimirEstoque();               
          break;              
        case 3:              
          return;              
        default:                 
          System.out.println("Opção inválida.");          
      }       
    }  
  }   
  private static void gerenciarEstoque(Scanner scanner, Estoque estoque) {    
    // funcao p gerenciar estoque    
    while (true) {       
      System.out.println("O que você deseja fazer?");        
      System.out.println("1. Adicionar produto");      
      System.out.println("2. Remover produto");       
      System.out.println("3. Atualizar produto");        
      System.out.println("4. Ver estoque");       
      System.out.println("5. Voltar");       
      int opcaoGerente = Integer.parseInt(scanner.nextLine());        
      switch (opcaoGerente) {          
        case 1:           
          adicionarProduto(scanner, estoque);              
          break;             
        case 2:            
          removerProduto(scanner, estoque);            
          break;             
        case 3:           
          atualizarProduto(scanner, estoque); 
          break;             
        case 4:            
          estoque.imprimirEstoque();           
          break;             
        case 5:               
          return;             
        default:               
          System.out.println("Opção inválida.");         
      }      
    }  
  }
  private static void adicionarProduto(Scanner scanner, Estoque estoque) {    //função para adicionar um produto        
    System.out.println("Digite o nome do produto:");      
    String nomeProduto = scanner.nextLine();       
    System.out.println("Digite o preço do produto:");     
    double precoProduto = Double.parseDouble(scanner.nextLine());     
    System.out.println("Digite a quantidade do produto:");      
    int quantidadeProduto = Integer.parseInt(scanner.nextLine());     
    int codigoProduto = estoque.gerarCodigoProduto(); // Gera um código automaticamente       
    Produto produto = new Produto(codigoProduto, nomeProduto, precoProduto, quantidadeProduto);       
    estoque.adicionarProduto(produto);        
    System.out.println("Produto adicionado com sucesso!");
  }    
  private static void removerProduto(Scanner scanner, Estoque estoque) {      //função para remover um produto       
    System.out.println("Digite o código do produto que deseja remover:");
    int codigoProduto = Integer.parseInt(scanner.nextLine());       
    estoque.removerProduto(codigoProduto);      
    System.out.println("Produto removido com sucesso!");
    }  
  private static void atualizarProduto(Scanner scanner, Estoque estoque) {       //função para atualizar o produto
      
    System.out.println("Digite o código do produto que deseja atualizar:");      
    int codigoProduto = Integer.parseInt(scanner.nextLine());      
    Produto produto = estoque.buscarProduto(codigoProduto);       
    if (produto != null) {          
      System.out.println("Digite o novo nome do produto:");         
      String novoNome = scanner.nextLine();        
      System.out.println("Digite o novo preço do produto:");         
      double novoPreco = Double.parseDouble(scanner.nextLine());      
      System.out.println("Digite a nova quantidade do produto:");     
      int novaQuantidade = Integer.parseInt(scanner.nextLine());      
      produto.setNome(novoNome);          
      produto.setPreco(novoPreco);          
      produto.setQuantidade(novaQuantidade);         
      System.out.println("Produto atualizado com sucesso!");      
    } else {        
      System.out.println("Produto não encontrado.");   
    } 
  } 
  private static void realizarVendas(Scanner scanner, Estoque estoque, Caixa caixa) {
    caixa.abrirCaixa();  
    while (true) {  
      System.out.println("Digite o código do produto que deseja comprar (ou 0 para voltar):");  
      int codigoProduto = Integer.parseInt(scanner.nextLine());  
      if (codigoProduto == 0) break;  
      System.out.println("Digite a quantidade do produto que deseja comprar:");   
      int quantidadeProduto = Integer.parseInt(scanner.nextLine());   
      Produto produto = estoque.buscarProduto(codigoProduto);  
      if (produto != null) {   
        if (produto.getQuantidade() >= quantidadeProduto) {
          caixa.adicionarCompra(produto, quantidadeProduto);
          produto.setQuantidade(produto.getQuantidade() - quantidadeProduto);
          System.out.println("Compra realizada com sucesso!");
          double valorTotal = produto.getPreco() * quantidadeProduto;
          System.out.println("Valor total da compra: R$" + valorTotal);      
        } else {      
          System.out.println("Quantidade insuficiente em estoque.");  
        }      
      } else {      
        System.out.println("Produto não encontrado.");     
      }
    }
  }
}





