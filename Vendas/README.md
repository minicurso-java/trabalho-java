# Sistema de Vendas

O Sistema de Vendas é um projeto prático que simula um sistema de vendas real. Ele aplica os conceitos de herança, polimorfismo, abstração e encapsulamento.

## Classes

O sistema contém as seguintes classes:

- **Produto**: Esta classe representa um produto à venda. Ela contém atributos como nome do produto, ID do produto, preço, quantidade em estoque, etc.

- **Cliente**: Esta classe representa um cliente. Ela contém atributos como nome do cliente, ID do cliente, compras realizadas, etc.

- **Venda**: Esta classe representa uma venda. Ela contém atributos como ID da venda, produtos vendidos, cliente, total da venda, etc.

## Funcionalidades

O sistema fornece as seguintes funcionalidades:

- **Adicionar produtos**: Os produtos podem ser adicionados ao sistema. O sistema registra o produto e atualiza a lista de produtos disponíveis.

- **Fazer vendas**: As vendas podem ser realizadas para os clientes. O sistema registra a venda e atualiza a quantidade de produtos em estoque.

- **Gerar faturas**: As faturas podem ser geradas para as vendas. O sistema retorna uma fatura com os detalhes da venda.

- **Gerenciamento de clientes**: O sistema permite adicionar, atualizar e remover clientes.

- **Gerenciamento de produtos**: O sistema permite adicionar, atualizar e remover produtos.

## Como usar

1. Inicie o sistema.
2. Selecione uma opção do menu principal.
3. Siga as instruções na tela para usar cada funcionalidade.