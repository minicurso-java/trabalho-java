class Produto {             //criamos a classe produto
    private int codigo;     //atributo codigo
    private String nome;    //atributo nome
    private double preco;   //atributo preço
    private int quantidade; //atributo quantidade

    public Produto(int codigo, String nome, double preco, int quantidade) {         //criação do método Produto
        this.codigo = codigo;
        this.nome = nome;
        this.preco = preco;
        this.quantidade = quantidade;
    }

    public int getCodigo() {            //criação do método getCodigo()
        return codigo;
    }

    public String getNome() {           //criação do método getnome()
        return nome;
    }

    public double getPreco() {          //criação do método getPreco()
        return preco;
    }

    public int getQuantidade() {        //criação do método getQuantidade()
        return quantidade;
    }

    public void setNome(String nome) {  //cração do método setNome
        this.nome = nome;
    }

    public void setPreco(double preco) { //criação do método setPreco
        this.preco = preco;
    }

    public void setQuantidade(int quantidade) { //criação do método setQuantidade
        this.quantidade = quantidade;
    }
}