class Caixa {           //criação da classe caixa
    private double totalVendas;         //atributo totalVendes

    public Caixa() {    //criação do método Caixa
        this.totalVendas = 0.0;
    }

    public void abrirCaixa() {      //criação do método abrirCaixa
        this.totalVendas = 0.0;
    }

    public void adicionarCompra(Produto produto, int quantidade) {          //criação do método adicionarCompra
        double valorCompra = produto.getPreco() * quantidade;
        this.totalVendas += valorCompra; // Guarda o valor total das vendas
    }

    public double getTotalVendas(){
        return totalVendas; // Função que retorna o total das vendas
    }
}