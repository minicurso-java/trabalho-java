# Sistema de Gerenciamento de Contas Bancárias

O Sistema de Gerenciamento de Contas Bancárias é um projeto prático que simula um sistema bancário real. Ele aplica os conceitos de herança, polimorfismo, abstração e encapsulamento.

## Classes

O sistema contém as seguintes classes:

- **Conta**: Esta classe representa uma conta bancária. Ela contém atributos como número da conta, titular da conta, saldo, etc.

- **Transação**: Esta classe representa uma transação bancária. Ela contém atributos como ID da transação, conta de origem, conta de destino, valor, data da transação, etc.

## Funcionalidades

O sistema fornece as seguintes funcionalidades:

- **Abertura de contas**: Os clientes podem abrir contas bancárias. O sistema registra a conta e atualiza a lista de contas.

- **Realização de transações**: Os clientes podem realizar transações, como depósitos, saques e transferências. O sistema registra a transação e atualiza o saldo das contas envolvidas.

- **Consulta de saldo**: Os clientes podem consultar o saldo de suas contas. O sistema retorna o saldo atual da conta.

- **Gerenciamento de contas**: O sistema permite adicionar, atualizar e remover contas.

## Como usar

1. Inicie o sistema.
2. Selecione uma opção do menu principal.
3. Siga as instruções na tela para usar cada funcionalidade.