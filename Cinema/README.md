# Sistema de Gerenciamento de Cinema

O Sistema de Gerenciamento de Cinema é um projeto prático que simula um sistema de cinema real. Ele aplica os conceitos de herança, polimorfismo, abstração e encapsulamento.

## Classes

O sistema contém as seguintes classes:

- **Filme**: Esta classe representa um filme no cinema. Ela contém atributos como título, diretor, duração, classificação etária, etc.

- **Sessão**: Esta classe representa uma sessão de cinema. Ela contém atributos como ID da sessão, filme sendo exibido, horário da sessão, sala de exibição, etc.

- **Bilhete**: Esta classe representa um bilhete de cinema. Ela contém atributos como ID do bilhete, sessão, assento, preço, etc.

## Funcionalidades

O sistema fornece as seguintes funcionalidades:

- **Compra de bilhetes**: Os clientes podem comprar bilhetes para as sessões de cinema. O sistema verifica a disponibilidade de assentos e, em seguida, emite o bilhete.

- **Programação de sessões**: O cinema pode programar sessões para os filmes. O sistema registra a sessão e atualiza a programação do cinema.

- **Gerenciamento de filmes**: O sistema permite adicionar, atualizar e remover filmes.

- **Gerenciamento de sessões**: O sistema permite adicionar, atualizar e remover sessões.

## Como usar

1. Inicie o sistema.
2. Selecione uma opção do menu principal.
3. Siga as instruções na tela para usar cada funcionalidade.