# Sistema de Gerenciamento de Transporte

O Sistema de Gerenciamento de Transporte é um projeto prático que simula um sistema de transporte real. Ele aplica os conceitos de herança, polimorfismo, abstração e encapsulamento.

## Classes

O sistema contém as seguintes classes:

- **Veículo**: Esta classe representa um veículo no sistema de transporte. Ela contém atributos como tipo de veículo, ID do veículo, capacidade, status (disponível/ocupado), etc.

- **Passageiro**: Esta classe representa um passageiro. Ela contém atributos como nome do passageiro, ID do passageiro, histórico de viagens, etc.

- **Viagem**: Esta classe representa uma viagem. Ela contém atributos como ID da viagem, veículo utilizado, passageiro, data e hora da viagem, origem, destino, etc.

## Funcionalidades

O sistema fornece as seguintes funcionalidades:

- **Agendamento de viagens**: Os passageiros podem agendar viagens. O sistema verifica a disponibilidade do veículo e, em seguida, registra a viagem.

- **Histórico de viagens**: Os passageiros podem visualizar seu histórico de viagens. O sistema retorna uma lista de viagens realizadas pelo passageiro.

- **Gerenciamento de passageiros**: O sistema permite adicionar, atualizar e remover passageiros.

- **Gerenciamento de veículos**: O sistema permite adicionar, atualizar e remover veículos.

## Como usar

1. Inicie o sistema.
2. Selecione uma opção do menu principal.
3. Siga as instruções na tela para usar cada funcionalidade.