# Atividade Prática: Projetos Orientados a Objetos em Java

Este repositório contém a atividade prática para a aula de Orientação a Objetos em Java. A atividade consiste em desenvolver um projeto prático que simule um sistema real, aplicando os conceitos de herança, polimorfismo, abstração e encapsulamento.

## Temas de Projetos

Aqui estão algumas ideias de projetos que você pode escolher:

- **Sistema de Biblioteca**

- **Sistema de Gerenciamento de Estudantes**

- **Sistema de Vendas**

- **Sistema de Reservas de Hotel**

- **Sistema de Gerenciamento de Funcionários**

- **Sistema de Gerenciamento de Contas Bancárias**

- **Sistema de Gerenciamento de Restaurante**

- **Sistema de Gerenciamento de Estoque**

- **Sistema de Gerenciamento de Transporte**

- **Sistema de Gerenciamento de Cinema**

Cada sistema terá diferentes classes e funcionalidades associadas. Por exemplo, um Sistema de Gerenciamento de Cinema pode ter classes como Filme, Sala e Ingresso, e funcionalidades como reserva de ingressos e exibição de filmes.

## Tempo de Desenvolvimento

Você terá 2 horas para desenvolver seu projeto. Lembre-se de aplicar os conceitos de herança, polimorfismo, abstração e encapsulamento em seu código. Boa sorte!
