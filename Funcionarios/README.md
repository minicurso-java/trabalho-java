# Sistema de Gerenciamento de Funcionários

O Sistema de Gerenciamento de Funcionários é um projeto prático que simula um sistema de gerenciamento de funcionários real. Ele aplica os conceitos de herança, polimorfismo, abstração e encapsulamento.

## Classes

O sistema contém as seguintes classes:

- **Funcionário**: Esta classe representa um funcionário. Ela contém atributos como nome, ID do funcionário, cargo, salário, etc.

- **Departamento**: Esta classe representa um departamento na empresa. Ela contém atributos como nome do departamento, ID do departamento, lista de funcionários, etc.

## Funcionalidades

O sistema fornece as seguintes funcionalidades:

- **Adicionar funcionários**: Os funcionários podem ser adicionados ao sistema. O sistema registra o funcionário e atualiza a lista de funcionários.

- **Atribuir funcionários a departamentos**: Os funcionários podem ser atribuídos a um departamento. O sistema registra a atribuição e atualiza a lista de funcionários do departamento.

- **Gerenciamento de funcionários**: O sistema permite adicionar, atualizar e remover funcionários.

- **Gerenciamento de departamentos**: O sistema permite adicionar, atualizar e remover departamentos.

## Como usar

1. Inicie o sistema.
2. Selecione uma opção do menu principal.
3. Siga as instruções na tela para usar cada funcionalidade.