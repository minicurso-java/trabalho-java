# Sistema de Gerenciamento de Estoque

O Sistema de Gerenciamento de Estoque é um projeto prático que simula um sistema de estoque real. Ele aplica os conceitos de herança, polimorfismo, abstração e encapsulamento.

## Classes

O sistema contém as seguintes classes:

- **Produto**: Esta classe representa um produto no estoque. Ela contém atributos como nome do produto, ID do produto, quantidade em estoque, preço, etc.

- **Transação**: Esta classe representa uma transação de estoque. Ela contém atributos como ID da transação, produto, quantidade, tipo de transação (entrada/saída), data da transação, etc.

## Funcionalidades

O sistema fornece as seguintes funcionalidades:

- **Adicionar produtos ao estoque**: Os produtos podem ser adicionados ao estoque. O sistema registra o produto e atualiza a quantidade de produtos em estoque.

- **Realizar transações de estoque**: As transações de estoque, como entrada e saída de produtos, podem ser realizadas. O sistema registra a transação e atualiza a quantidade de produtos em estoque.

- **Consulta de estoque**: A quantidade atual de um produto em estoque pode ser consultada. O sistema retorna a quantidade atual do produto em estoque.

- **Gerenciamento de produtos**: O sistema permite adicionar, atualizar e remover produtos.

## Como usar

1. Inicie o sistema.
2. Selecione uma opção do menu principal.
3. Siga as instruções na tela para usar cada funcionalidade.